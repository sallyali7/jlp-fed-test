import ProductCarousel from "../../components/product-carousel/product-carousel";
import styles from "../product-detail/detail.module.scss";

export async function getServerSideProps(context) {
  const id = context.params.id;
  const response = await fetch(
    "https://api.johnlewis.com/mobile-apps/api/v1/products/" + id
  );
  const data = await response.json();

  return {
    props: { data },
  };
}

const ProductDetail = ({ data }) => {
  const productInformation = data.details.productInformation
  return (
    <section>
<div className={styles.heading}>
      <h1><div className={styles.productHeader} dangerouslySetInnerHTML={{ __html: data.title }} /></h1>
    </div>
    <div className={styles.productContainer}>
        <div className={styles.offers}>
        <h1 className={styles.priceNow}>£{data.price.now}</h1>
        <h6 className={styles.specialOffer}>{data.displaySpecialOffer}</h6>
        <h5>{data.additionalServices.includedServices}</h5>
        </div>
        <div className={styles.feat}>
        <ProductCarousel image={data.media.images.urls[0]} />
        {/* <ProductCarousel image={data.media.images.urls[1]} />
    <ProductCarousel image={data.media.images.urls[2]} /> */}
        <h3>Product information</h3>
        <h4>Product Code: {data.code}</h4>
        <div className={styles.information}><div dangerouslySetInnerHTML={{ __html: productInformation }} /></div>
        <h3 className={styles.productSpecification}>Product specification</h3>
        <div className={styles.specification}>
          {data.details.features[0].attributes.map((item) => (
            <><div className={styles.specificationName}><div dangerouslySetInnerHTML={{ __html: item.name }} /></div>
              <div className={styles.specificationValue}><div dangerouslySetInnerHTML={{ __html: item.value }} /></div></>
          ))}
        </div>
        </div>      
    </div>
    </section>
      
  );
};

export default ProductDetail;
