# UI Dev Technical Test - Dishwasher App

## Brief

When running Npm i, I ran into a few issues with node-sass and it's compatabiity with my local version of Node. I resorted to updating some configurations and the combination of versions work. So to run this file on your machine and run it correctly, install the dependencies below:

    • jest: ^26.6.3
    • lodash: ^4.17.21
    • next: ^12.1.5
    • node-fetch: ^3.0.0
    • node-sass: ^7.0.1
    • react: 17.0.1
    • react-dom: 17.0.1
    • Node: v18.0.0


### Product Grid

I uncommented the fetch request from the Api which displayed the Dishwasher grid on the homepage, I chose slice to select only the first 20 items from the data array as that was the task at hand and returned te ProductListItem imported to display the diswashers. ProductListItem is wrapped in a Link component which enables us to transition to the route of an individual dishwasher selected. 


### Product Page

When a dishwasher is clicked, a new screen is displayed showing the dishwasher’s details. An Async function is called and we await the result of the api url plus the products ID. The data is then returned to us as a json file. 

### Designs

In the `/designs` folder you will find 3 images that show the desired screen layout for the app

- product-page-portrait.png
- product-page-landscape.png
- product-grid.png

### Testing

I used Jest and React Testing Libraries. I mocked axios and used it along with the mocked data to imitate the network requests and the data returned. 

## Things we're looking for

- Unit tests are important. We’d like to see a TDD approach to writing the app. We've included a Jest setup.
- The website should be fully responsive, working across device sizes. We've provided you with some ipad-sized images as a guide.
- The use of third party code/SDKs is allowed, but you should be able to explain why you have chosen the third party code.
- Put all your assumptions, notes, instructions and improvement suggestions into your GitHub README.md.
- We’re looking for a solution that's as close to the designs as possible.
- We'll be assessing your coding style, how you've approached this task and whether you've met quality standards on areas such as accessibility, security and performance.
- We don't expect you to spend too long on this, as a guide 3 hours is usually enough.

---

## Getting Started

- `Fork this repo` into your own GitLab namespace (you will need a GitLab account).
- Install the NPM dependencies using `npm i`. (You will need to have already installed NPM using version `14.x`.)
- Run the development server with `npm run dev`
- Open [http://localhost:3000](http://localhost:3000) with your browser.
